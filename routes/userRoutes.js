const express = require("express");
const router = express.Router();
const UserController = require("../controllers/userControllers")
const auth = require("../auth");


router.post("/register", (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result));
});

// router.post("/login", (req, res) => {
// 	UserController.loginUser(req.body).then(result => res.send(result));
// });

// router.get("/details", auth.verify, (req, res) => {
// 	const userData = auth.decode(req.headers.authorization);

// 	UserController.getProfile(userData.id).then(result => res.send(result))

// })


// router.post("/enroll", auth.verify, (req, res) => {
// 	let data = {
// 		userId : auth.decode(req.headers.authorization).id,
// 		courseId : req.body.courseId
// 	}

// 	UserController.enroll(data).then(result => res.send(result));
	
	
// })


module.exports = router;